﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        public InMemoryRepository()
        {
            
        }
        protected static IEnumerable<T> Data { get; set; }
        
        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> PostAsync(T entity)
        {
            var dataList = new List<T>();
            if (Data != null)
            {
                dataList = Data.ToList();
            }
            
            dataList.Add(entity);
            Data = dataList.Select(x => x);

            return Task.FromResult(entity);
        }

        public Task<T> PatchAsync(Guid id, T entity)
        {
            if (Data == null)
            {
                throw new NullReferenceException(nameof(Data));
            }

            if (Data.All(x => x.Id != id))
            {
                throw new Exception("No data found with such id.");
            }

            Data = Data
                .Where(x => x.Id == id)
                .Select(x => { x = entity; return x; });

            return Task.FromResult(entity);
        }

        public Task DeleteAsync(Guid id)
        {
            if (Data == null)
            {
                throw new NullReferenceException(nameof(Data));
            }

            if (Data.All(x => x.Id != id))
            {
                throw new Exception("No data found with such id.");
            }

            Data = Data.Where(x => x.Id != id);
            
            return Task.FromResult(Data);
        }
    }
}